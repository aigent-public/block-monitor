# block-monitor

Create monitoring 

(Implemented: Prometheus)


## Usage example: 

```
const os = require('os');

const BlockMonitor = require('block-monitor').BlockMonitor;
const PROMETHEUS = require('block-monitor').PROMETHEUS;

const bm = new BlockMonitor();

const options = {
    name: os.hostname(),
    port: 3000
};

console.debug(options);

const monitor = bm.createMonitor(PROMETHEUS, options);

let ticks = monitor.createCounter({
    name: "numberOfTicks",
    help: "Number of Ticks",
});


setInterval(() => {
    ticks.inc();
});


```
 
## Run Tests

npm test
npm run-script test-cov
