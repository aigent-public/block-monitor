'use strict';


const prometheusClient = require('prom-client');
const express = require('express');
const os = require('os');

class Monitor {
}

class PrometheusMonitor {

    constructor(options) {

        options.name = (options.name || os.hostname()).replace("-", "_");
        options.port = parseInt(options.port || 3000);

        const register = prometheusClient.register;
        register.setDefaultLabels({"block_name": [options.name]});

        const server = express();
        // noinspection JSUnresolvedFunction
        server.get('/metrics', (req, res) => {
            res.set('Content-Type', register.contentType);
            res.end(register.metrics());
        });

        server.listen(options.port);
    }

    createDefaultMetrics() {
        return new prometheusClient.collectDefaultMetrics();
    }

    createGauge(options) {
        return new prometheusClient.Gauge(options);
    }

    createCounter(options) {
        return new prometheusClient.Counter(options);
    }

    createHistogram(options) {
        return new prometheusClient.Histogram(options);
    }

}

module.exports = PrometheusMonitor;
