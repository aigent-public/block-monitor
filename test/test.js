'use strict';

const os = require('os');

const BlockMonitor = require('../index').BlockMonitor;
const PROMETHEUS = require('../index').PROMETHEUS;

const bm = new BlockMonitor();

const options = {
    name: os.hostname(),
    port: 3000
};

console.debug(options);

const monitor = bm.createMonitor(PROMETHEUS, options);

let ticks = monitor.createCounter({
    name: "numberOfTicks",
    help: "Number of Ticks",
});


setInterval(() => {
    ticks.inc();
});

