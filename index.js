// Create an instance of our factory that makes cars

/**
 * Usage:

 const MonitorFactory = new MonitorFactory();
 const prometheus = MonitorFactory.MonitorFactory('prometheus',{...});

 */

const PROMETHEUS = 'prometheus';

class MonitorFactory {
}

MonitorFactory.prototype.createMonitor = (type, options) => {

    switch (type) {
        case PROMETHEUS :
            this.monitorClass = require('./src/prometheus-monitor');
            break;
        default:
            this.monitorClass = require('./src/prometheus-monitor');
            break;
    }

    return new this.monitorClass(options);
};

exports.BlockMonitor = MonitorFactory;
exports.PROMETHEUS = PROMETHEUS;
